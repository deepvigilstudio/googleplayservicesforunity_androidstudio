package com.deepvigil.gpg;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.nfc.Tag;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.leaderboard.LeaderboardScoreBuffer;
import com.google.android.gms.games.leaderboard.Leaderboards;
import com.google.games.basegameutils.GameHelper;
import com.unity3d.player.UnityPlayer;

public class GooglePlayGamesPlugin extends Fragment
        implements
//        GoogleApiClient.ConnectionCallbacks,
//        GoogleApiClient.OnConnectionFailedListener,
        GameHelper.GameHelperListener
{
    private static  GoogleApiClient client;
    private static GameHelper gameHelper;

    private static String TAG = "GooglePlayServicesUnity";
    private static GooglePlayGamesPlugin instance;

    public static void Initialize(){

        instance = new GooglePlayGamesPlugin();
        gameHelper = new GameHelper(UnityPlayer.currentActivity, GameHelper.CLIENT_GAMES);
        gameHelper.setup(instance);
        gameHelper.setMaxAutoSignInAttempts(0);

        UnityPlayer.currentActivity
                .getFragmentManager()
                .beginTransaction()
                .add(instance, "GMGUnityPlugin")
                .commit();
    }


    @Override
    public void onStart() {
        super.onStart();
        gameHelper.onStart(UnityPlayer.currentActivity);
    }

    @Override
    public void onStop() {
        super.onStop();
        gameHelper.onStop();
    }

    public static void SubmitScore(int score, String leaderboardId) {
        if (gameHelper.isSignedIn()) {
            Log.d(TAG, "SubmitScore");
            Games.Leaderboards.submitScore(gameHelper.getApiClient(),
                    leaderboardId, score);
        }
        else{
            Log.d(TAG, "SubmitScore Error: isSignedIn is false");
            Connect();
        }
    }

    public static void DisplayLeaderboard(String leaderboardId) {
        if (gameHelper.isSignedIn()) {
            Log.d(TAG, "DisplayLeaderboard");
            instance.startActivityForResult(
                    Games.Leaderboards.getLeaderboardIntent(
                            gameHelper.getApiClient(),
                            leaderboardId),
                    24);
        }
        else{
            Log.d(TAG, "DisplayLeaderboard Error: isSignedIn is false");
            Connect();
        }
    }

    public static void DisplayAchievements() {
        if (gameHelper.isSignedIn()) {
            Log.d(TAG, "DisplayAchievements");
            instance.startActivityForResult(
                    Games.Achievements.getAchievementsIntent(
                            gameHelper.getApiClient()),
                    25);
        }
        else{
            Log.d(TAG, "DisplayAchievements Error: isSignedIn is false");
            Connect();
        }
    }

    public static void Connect(){
        if(!gameHelper.isSignedIn()) {
            Log.d(TAG, "Connect: signing in");
            gameHelper.reconnectClient();
        }
        else{
            Log.d(TAG, "Connect: user is already signed in");
        }
    }

    public static void Disconnect(){
        Log.d(TAG, "Disconnect");
        gameHelper.disconnect();
    }


    //________________  Interfaces Beyond This Point    ________________


    //NOTE for some reason UnitySendMessage is crashing the game
    //it's not vital, removing temporarily

    @Override
    public void onSignInFailed() {
        Log.d(TAG, "onSignInFailed");
        //UnityPlayer.UnitySendMessage("GooglePlayGamesPlugin", "OnSignInFailed", null);
    }

    @Override
    public void onSignInSucceeded() {
        Log.d(TAG, "onSignInSucceeded");
        //UnityPlayer.UnitySendMessage("GooglePlayGamesPlugin", "OnSignInSucceeded", null);
    }
}
